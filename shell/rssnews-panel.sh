#!/usr/bin/env bash
# Dependencies: rsstail
# See also: $HOME/scripts/shell/archnews

# Optional icon to display before the text
# Insert the absolute path of the icon
# Recommended size is 24x24 px
readonly ICON="$HOME/.local/share/icons/xfce4-genmon/network/web.png"

# Calculate updates
readonly FILE=/tmp/archnews
readonly TITLE_SHORT=$(cat $FILE | grep Title | tail -c +8 | head -c 50)
readonly DATE=$(cat $FILE | grep Pub | tail -c +11 | head -c +16)
readonly TITLE_LONG=$(cat $FILE | grep Title)
readonly DETAILS=$(cat $FILE | grep Descrip)

# Panel
if [[ $(file -b "${ICON}") =~ PNG|SVG ]]; then
  INFO="<img>${ICON}</img>"
  INFO+="<txt>"
else
  INFO="<txt>"
  INFO+="Icon is missing!"
fi
#INFO+="${TITLE_SHORT}"
INFO+="</txt>"

# Tooltip
MORE_INFO="<tool>"
MORE_INFO+="${DATE}\n"
MORE_INFO+="${TITLE_LONG}\n"
MORE_INFO+="${DETAILS}"
MORE_INFO+="</tool>"

# Panel Print
echo -e "${INFO}"

# Tooltip Print
echo -e "${MORE_INFO}"
