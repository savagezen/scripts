#!/bin/sh
# lock user cryptvol if not logged in; because
# pam unmounts on logout, but device left mapped by dm-crypt.
# set cron to run as root every x minutes.

for u in $(who | awk '{print $1}')
  do
    if [ $? -eq 0 ]; then
#      echo "user logged in"
      exit 0
    else
#      echo "user not logged in"
      cryptsetup close /dev/mapper/home-$u
    fi
  done
